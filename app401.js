// const Laptops = ["Asus", "Acer", "Msi", "Hp", "Dell", "Gigabyte", "Lenovo"]
const Weeks = ["1", "2", "3", "4", "5", "6", "7"]
// Destructuring Assignment

// const [first,,second,,,,third] = Laptops

// console.log(first, second, third);

// Destructuring Assignment with rest operator
const [week1, week2, ...args] = Weeks
console.log(week1)
console.log(week2)
console.log(args)
