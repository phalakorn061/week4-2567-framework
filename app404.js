// Spread Operator
const odd = [1, 3, 5];
const odd2 = [7, 9, 11];
const combinded = [...odd, 2, 4, 6, ...odd2];
console.log(combinded);

function sum(a, b, ...args) {
    console.log(a, b, args)
}
sum(1, 2, 3, 4, 5)