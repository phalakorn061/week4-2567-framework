// Spread : Merg / Concat
const a1 = [1, 2, 3];
const a2 = [4, 5, 6];
const a3 = 7;
const user1 = { name: "G B" };
const user2 = { photo: "N/A.jpeg" };

const arrC = [...a1, ...a2, a3];
console.log(arrC);

const users = { ...user1, ...user2 };
const { user } = users;
console.log(users);
