// define Object
const users = [
  {
    fname: "Mr.Sucker Bu",
    address: "Trat 1",
    age: 60,
  },
  {
    fname: "Mr.G T",
    address: "Trat 2",
    age: 90,
  },
  {
    fname: "Mr.G G",
    address: "Trat 3",
    age: 40,
  },
];
const drinks = ["Coke", "Coke Zero", "Sprite"]
for (const drink in drinks) {
    console.log(`${drink} : ${drinks[drink]}`)
}

const fname = users[0].fname
const fname1 = users[1].fname
console.log(fname, fname1)

// Object Destructuring with for of loop
// for (const { fname, address, age, saraly = 10 } of users) {
for (const user of users) {
    // console.log(`Name: ${fname} Address: ${address} Age: ${age} Saraly: ${saraly}`)
    console.log(`
        Name: ${user.fname} Address: ${user.address} Age: ${user.age} Saraly: ${user.saraly}
        `)
}